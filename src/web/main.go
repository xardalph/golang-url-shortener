package main

import (
	"github.com/gin-gonic/gin"
	"github.com/ilyakaznacheev/cleanenv"
	"log"
	"web/controller"
	"web/model"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func SetupConfig() model.Conf {
	var cfg model.Conf

	err := cleanenv.ReadEnv(&cfg)
	if err != nil {
		log.Fatal(err)
	}

	return cfg
}

func loadHtmlTemplate(r *gin.Engine) {
	r.Static("/css", "./static/css")
	r.StaticFile("/favicon.ico", "./img/favicon.ico")
	r.Static("/scss", "./static/scss")
	r.Static("/vendor", "./static/vendor")
	//r.Static("/js", "./static/js")
	r.Static("/img", "./static/img")

	r.LoadHTMLGlob("templates/**/*")
}

func main() {
	cfg := SetupConfig()
	r := gin.New()
	// gin.SetMode(gin.ReleaseMode)
	r.Use(gin.Logger())

	r.Use(gin.Recovery())

	loadHtmlTemplate(r)
	controller.Router(r, cfg)

	log.Println("Server started")
	err := r.Run()
	if err != nil {
		log.Fatal(err)
	} // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
