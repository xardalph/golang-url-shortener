package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"web/model"
)

type DB struct {
	redis *redis.Client
}

func GetDatabaseHandle(cfg model.Conf) DB {
	Redis := redis.NewClient(&redis.Options{
		Addr:     cfg.DBHost + ":" + cfg.DBPort,
		Password: cfg.DBPassword,
		DB:       0,
	})

	pong, err := Redis.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(pong)
	return DB{redis: Redis}
}

func Router(r *gin.Engine, cfg model.Conf) {
	db := GetDatabaseHandle(cfg)
	r.GET("/", db.index)
	r.POST("/save", db.addUrl)
	r.GET("/s/:url", db.redirect)
}
func (db DB) redirect(c *gin.Context) {
	url := c.Param("url")
	fmt.Println(url)
	val, err := db.redis.Get(url).Result()
	if err == redis.Nil {

	} else if err != nil {

	}
	c.Redirect(http.StatusFound, val)
}

// constant used to define allowed value for a random url, we could add number to it.
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

// RandStringBytes
// It would be better to check for url collision before using a key,
// with the size of key use for now (10) it's a very low probability.
func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

type Form struct {
	Url string `form:"url" binding:"required"`
}

func (db DB) addUrl(c *gin.Context) {

	var UrlParam Form
	if err := c.ShouldBind(&UrlParam); err != nil {
		fmt.Println("ERROR binding data : " + err.Error())
		c.HTML(
			http.StatusBadRequest,
			"views/index.html",
			gin.H{
				"title": "Project",
				"error": "failed to parse POST data, please check you send the right data form. " + err.Error(),
			},
		)
	}
	log.Println(UrlParam.Url)

	randomString := RandStringBytes(10)
	fullUrl := c.Request.Host + "/s/" + randomString
	fmt.Printf("UrlParam to save : %s \nUrlParam created : %s ", UrlParam.Url, randomString)
	fmt.Println("host UrlParam is : " + c.Request.Host + "/s/" + randomString)
	err := db.redis.Set(randomString, UrlParam.Url, 0).Err()
	if err != nil {

		c.HTML(
			http.StatusBadRequest,
			"views/index.html",
			gin.H{
				"title": "Project",
				"error": "failed to save data in Database, please contact support.",
			},
		)
	} else {

		c.Redirect(http.StatusFound, "/?message="+url.QueryEscape(fullUrl))
	}
}

type Message struct {
	Message string `form:"message"`
}

func (db DB) index(c *gin.Context) {
	var previousMessage Message
	_ = c.ShouldBind(&previousMessage)
	// this field is empty if the client don't come from a redirect of addUrl()

	c.HTML(
		http.StatusOK,
		"views/index.html",
		gin.H{
			"title":   "Project",
			"message": previousMessage.Message,
		})

}
