package model

type Conf struct {
	DBHost     string `env:"REDIS_HOST" env-default:"redis"`
	DBPort     string `env:"REDIS_PORT" env-default:"6379"`
	DBName     string `env:"DBNAME" env-default:"urlShortener"`
	DBUser     string `env:"REDIS_USER" env-default:"root"`
	DBPassword string `env:"REDIS_PASSWORD" env-default:""`
}
