# Golang url shortener

a simple url shortener made to learn Golang and related technologies

## Build
```
cd src/web
go build
```

## Contribution
As it is only a pet project contribution are not welcome for now.

For production grade url shortener see bit.ly or shorturl.at.
